# Abdal Packet Sniffer

Simple and fast Packet Sniffer for windows

 

## Requirement
Microsoft Visual Studio



### About Programmer
Ebrahim Shafiei from Iran (Ready to cooperate with international projects)
  - Email : Prof.Shafiei@Gmail.com
  - WebSite https://hackers.zone/


## License
Abdal Packet Sniffer is open-source software licensed under the [MIT license.](https://choosealicense.com/licenses/mit/)

## ❤️ Donation
> USDT:      TXLasexoQTjKMoWarikkfYRYWWXtbaVadB

> bitcoin:   19LroTSwWcEBY2XjvgP6X4d6ECZ17U2XsK

> For Iranian People -> MellatBank : 6104-3378-5301-4247


## ⚠️ Legal disclaimer ⚠️

Usage of Abdal Packet Sniffer for attacking targets without prior mutual consent is illegal. It's the end user's responsibility to obey all applicable local, state and federal laws. Developers assume no liability and are not responsible for any misuse or damage caused by this program.

